import torch
from torchvision import datasets, transforms
import torch.nn as nn
from torch import  optim
class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(256*256, 100_0),
            nn.ReLU(),
            nn.Linear(100_0, 30_0),
            nn.ReLU(),
            nn.Linear(30_0, 10_000),
            nn.ReLU(),
            nn.Linear(10_000, 3),
            nn.ReLU(),
            nn.Softmax(dim=1)
        )


    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)

        return logits
