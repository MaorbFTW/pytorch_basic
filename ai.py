import torch
import cv2
import torchvision
from torchvision import transforms,datasets
import  AImodels
import  cnn
import numpy as np
vid = cv2.VideoCapture(0)
picsize=128
ret, frame = vid.read()

# Display the resulting frame
transform = transforms.Compose([transforms.Resize(picsize),
transforms.CenterCrop(picsize),
transforms.ToTensor()])

device = torch.device('cuda:0')
model = cnn.Net( ).to(device)
model.load_state_dict(torch.load("hazi2.pth"))
model.eval()
model.to(device)

dataset = datasets.ImageFolder("test", transform=transform)
dataloader = torch.utils.data.DataLoader(dataset, shuffle=True)
images,labels = next(iter(dataloader))
images = images.to(device)# 0 :right 1:? 2: stop
print (f"predict {model.forward(images)}")

"""
while (True):

    # Capture the video frame
    # by frame
    ret, frame = vid.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    resized = cv2.resize(gray, (256,256), interpolation = cv2.INTER_AREA)
    a=torch.tensor(resized,dtype=torch.float).to(device)
    
    print(a)
    y=model.forward(a.reshape((1,1,256,256)))

    print(y)
    cv2.imshow('frame', resized)


    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
        """