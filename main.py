
import torch
from torchvision import datasets, transforms
import AImodels
from torch import  nn
from torch import  optim
import cnn
from  time import sleep
picsize=200
device = torch.device('cuda:0')

print(f"Using {torch.cuda.is_available()} device")

# Define model

target= torch.tensor([[1,0,0],[0,1,0],[0,0,1]],dtype=torch.float ).to(device)


data_dir = "pic"
transform = transforms.Compose([transforms.Resize(picsize),
        transforms.CenterCrop(picsize),

        transforms.ToTensor()])
model= cnn.Net().to(device)
dataset = datasets.ImageFolder(data_dir, transform=transform)

epsos = 100_000

optimizer = optim.SGD(model.parameters(), lr=0.0001,momentum=0.9)
dataloader = torch.utils.data.DataLoader(dataset, batch_size=600, shuffle=True)
images, labels = next(iter(dataloader))
labels = labels.to(device)
images = images.to(device)
print(images.shape)

los = nn.MSELoss()

for epso in range(1,epsos+1):
    model.zero_grad()
    y= model.forward(images)
    loss= los(y,target[labels])
    loss.backward()
    optimizer.step()
    if epso %100==0:

        print(f"epso ={epso}  , loss= {loss}")


torch.save(model.state_dict(), "hazi2.pth")

